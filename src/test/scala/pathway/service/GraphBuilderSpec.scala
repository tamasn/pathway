package pathway.service

import org.specs2.Specification
import pathway.model._
import pathway.service.GraphBuilder.{ GraphValidationError, MismatchedConnection }

class GraphBuilderSpec extends Specification {
  def is = s2"""
  valid graph $validGraph
  mismatched connection $mismatchedConnection
  """

  val validSmallGraphDescription = List(
    (1, List(2, 3)),
    (2, List(4, 3)),
    (3, List(1, 4)),
    (4, List(2, 3)))

  val mismatchedSmallGraphDescription = List(
    (1, List(2, 3)),
    (2, List(4, 3)),
    (3, List(1, 3)),
    (4, List(2, 3)))

  val smallGraph = Graph(Map(
    NodeId(1) -> Node(NodeId(1), Map(
      NodeId(2) -> Disconnected,
      NodeId(3) -> Disconnected)),
    NodeId(2) -> Node(NodeId(2), Map(
      NodeId(4) -> Disconnected,
      NodeId(3) -> Disconnected)),
    NodeId(3) -> Node(NodeId(3), Map(
      NodeId(1) -> Disconnected,
      NodeId(4) -> Disconnected)),
    NodeId(4) -> Node(NodeId(4), Map(
      NodeId(2) -> Disconnected,
      NodeId(3) -> Disconnected))
  ))

  def validGraph = {
    val retVal: Either[GraphValidationError, Graph] = Right(smallGraph)
    GraphBuilder.validGraph(validSmallGraphDescription) must_== retVal
  }

  def mismatchedConnection = {
    val retVal: Either[GraphValidationError, Graph] = Left(MismatchedConnection(NodeId(4), NodeId(3)))
    GraphBuilder.validGraph(mismatchedSmallGraphDescription) must_== retVal
  }

}
