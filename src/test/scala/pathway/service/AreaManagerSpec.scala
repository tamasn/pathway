package pathway.service

import org.specs2.Specification
import pathway.model._

class AreaManagerSpec extends Specification {
  def is = s2"""
    continuous area $contArea
  """

  val testGraph: Option[Graph] = GraphBuilder.validGraph(List(
    (1, List(2, 4)),
    (2, List(3, 5, 1)),
    (3, List(2, 6)),
    (4, List(1, 5, 7)),
    (5, List(2, 6, 8, 4)),
    (6, List(3, 9, 5)),
    (7, List(4, 8)),
    (8, List(5, 9, 7)),
    (9, List(6, 8))
  )).toOption

  def contArea =
    testGraph.map(AreaManager.continuousArea(_, NodeId(5)).map(_.map(_.id.v))) must_== Some(Right(Set(1, 2, 3, 4, 5, 6, 7, 8, 9)))
}
