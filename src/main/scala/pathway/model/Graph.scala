package pathway.model

final case class NodeId(v: Int) extends AnyVal
final case class Graph(nodes: Map[NodeId, Node]) extends AnyVal {
  def setNode(n: Node): Graph = Graph(nodes + (n.id -> n))
}

object Graph {
  lazy val empty: Graph = Graph(Map.empty[NodeId, Node])
}
