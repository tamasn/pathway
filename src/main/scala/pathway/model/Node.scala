package pathway.model

final case class Node(
    id: NodeId,
    connections: Node.Connections) {
  def setConnection(id: NodeId, edge: Edge): Node = this.copy(connections = connections + (id -> edge))
  lazy val status: NodeStatus = connections.collectFirst({
    case (_, Connected) => NodeStatus.Connected
  }).getOrElse(NodeStatus.Empty)
}

sealed trait NodeStatus
object NodeStatus {
  case object Empty extends NodeStatus
  case object Connected extends NodeStatus
}

object Node {
  type Connections = Map[NodeId, Edge]
}
