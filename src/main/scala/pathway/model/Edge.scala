package pathway.model

sealed trait Edge

case object Connected extends Edge
case object Disconnected extends Edge

