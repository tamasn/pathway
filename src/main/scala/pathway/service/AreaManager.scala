package pathway.service

import cats.data.{ EitherT, State }
import pathway.model._
import pathway.service.GraphManager.{ getNode, emptyNode }

object AreaManager {
  type ACState[A] = State[Set[NodeId], A]
  type ACalc[A] = EitherT[ACState, ManagerError, A]

  def continuousArea(g: Graph, id: NodeId): ManagerResults[Set[Node]] =
    for {
      n <- getNode(g, id)
      area <- continuousArea0(g, n).value.runA(Set.empty).value
    } yield area

  private def continuousArea0(
    g: Graph,
    n: Node): ACalc[Set[Node]] = for {
    _ <- isNodeEmpty(n)
    isVisited <- isNodeVisited(n)
    area <- if (isVisited) emptyNodeList else for {
      _ <- makeNodeVisited(n)
      neighbors <- emptyNeighbors(g, n)
      visited <- getVisited
      area <- loopNeighbors(g, neighbors)
    } yield area
  } yield area + n

  private def emptyNeighbors(
    g: Graph,
    n: Node): ACalc[List[Node]] = EitherT[ACState, ManagerError, List[Node]](State.inspect(visited => {
    val z: Either[ManagerError, List[Node]] = Right(Nil)
    n.connections.foldLeft(z) {
      case (err @ Left(_), _) => err
      case (Right(acc), (id, _)) => for {
        n <- getNode(g, id)
      } yield if (n.status == NodeStatus.Empty && !visited.contains(n.id)) n :: acc else acc
    } match {
      case Left(err)  => Left(err)
      case Right(acc) => Right(acc.reverse)
    }
  }))

  private def isNodeEmpty(n: Node): ACalc[Unit] =
    EitherT[ACState, ManagerError, Unit](
      State.inspect(_ => emptyNode(n)))

  private def isNodeVisited(n: Node): ACalc[Boolean] =
    EitherT[ACState, ManagerError, Boolean](
      State.inspect(v => Right(v.contains(n.id)))
    )

  private def makeNodeVisited(n: Node): ACalc[Unit] =
    EitherT[ACState, ManagerError, Unit](State.modify[Set[NodeId]](v => v + n.id).map(_ => Right(())))

  private def getVisited: ACalc[Set[NodeId]] =
    EitherT[ACState, ManagerError, Set[NodeId]](State.get.map(Right.apply))

  private def emptyNodeList: ACalc[Set[Node]] =
    EitherT[ACState, ManagerError, Set[Node]](State.pure(Right(Set.empty)))

  private def loopNeighbors(
    g: Graph,
    ns: List[Node]): ACalc[Set[Node]] = ns match {
    case Nil => emptyNodeList
    case h :: t => for {
      area <- continuousArea0(g, h)
      rest <- loopNeighbors(g, t)
    } yield area ++ rest
  }

  type Connections = Set[NodeId]
  type Area = Set[NodeId]
  type ConnectionsByArea = List[(Area, Connections)]

  def addNodeIfInArea(connections: ConnectionsByArea, id: NodeId): Option[ConnectionsByArea] = {
    val (found, updatedConnections) = connections.foldLeft((false, Nil): (Boolean, ConnectionsByArea)) {
      case ((true, acc), i) => (true, i :: acc)
      case ((_, acc), i @ (area, conn)) =>
        if (area.contains(id)) (true, (area, conn + id) :: acc)
        else (false, i :: acc)
    }
    if (found) Some(updatedConnections.reverse)
    else None
  }

  def addNodeToCbA(
    g: Graph,
    connectionsByArea: ConnectionsByArea,
    id: NodeId): ManagerResults[ConnectionsByArea] = {
    addNodeIfInArea(connectionsByArea, id).map(Right.apply).getOrElse {
      continuousArea(g, id).map(nodes => (nodes.map(_.id), Set(id)) :: connectionsByArea)
    }
  }

  def availableConnectionsByArea(g: Graph, n: Node): ManagerResults[ConnectionsByArea] = {
    val zero: ManagerResults[ConnectionsByArea] = Right(Nil)
    n.connections.foldLeft(zero) {
      case (err @ Left(_), _) => err
      case (Right(acc), (id, _)) => for {
        n <- getNode(g, id)
        updatedCbA <- if (n.status == NodeStatus.Empty) addNodeToCbA(g, acc, id) else Right(acc)
      } yield updatedCbA
    }
  }

}
