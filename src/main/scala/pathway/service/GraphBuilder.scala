package pathway.service

import pathway.model._

import scala.annotation.tailrec

object GraphBuilder {
  import Node.Connections

  type GraphDescription = List[(Int, List[Int])]
  type ConnectionList = List[(NodeId, Edge)]

  sealed trait GraphValidationError
  final case class MismatchedConnection(leftHandId: NodeId, rightHandId: NodeId) extends GraphValidationError

  def validGraph(desc: GraphDescription): Either[GraphValidationError, Graph] = {
    validGraph0(Right(Graph.empty), desc, desc.headOption.map(_._2.size).getOrElse(0))
  }

  @tailrec
  private def validGraph0(
    acc: Either[GraphValidationError, Graph],
    d: GraphDescription,
    expectedConnectionCount: Int): Either[GraphValidationError, Graph] = (acc, d) match {
    case (acc, Nil)         => acc
    case (acc @ Left(_), _) => acc
    case (Right(acc), (id, connections) :: rest) =>
      validGraph0(addNode(acc, NodeId(id), connections), rest, expectedConnectionCount)
  }

  private def addNode(
    g: Graph,
    id: NodeId,
    connections: List[Int]): Either[GraphValidationError, Graph] = for {
    conns <- validateConnections(g, id, createConnections(connections).toMap)
    node = Node(id, conns)
  } yield Graph(g.nodes + (id -> node))

  private def createConnections(connections: List[Int]): ConnectionList =
    connections.map(id => (NodeId(id), Disconnected))

  private def validateConnections(
    g: Graph,
    id: NodeId,
    connections: Map[NodeId, Edge]): Either[GraphValidationError, Connections] = {
    val invalidConnection: Option[Either[GraphValidationError, Connections]] = connections.collectFirst {
      case (otherId, _) if !matchingConnection(g, id, otherId) =>
        Left(MismatchedConnection(id, otherId))
    }
    invalidConnection.getOrElse(Right(connections))
  }

  private def matchingConnection(
    g: Graph,
    originalId: NodeId,
    id: NodeId): Boolean = {
    g.nodes.get(id).map(_.connections.contains(originalId)).getOrElse(true)
  }
}

