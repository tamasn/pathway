package pathway

import pathway.model.{ Edge, NodeId }

package object service {
  type ManagerResults[A] = Either[ManagerError, A]

  sealed trait ManagerError
  final case class MissingNode(id: NodeId) extends ManagerError
  final case class MissingConnection(id: NodeId, otherId: NodeId) extends ManagerError
  final case class MismatchedConnection(edge: Edge, otherEdge: Edge) extends ManagerError
  final case class NotEmptyNode(id: NodeId) extends ManagerError

}
