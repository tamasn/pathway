package pathway.service

import pathway.model._

object GraphManager {

  def connect(g: Graph, id1: NodeId, id2: NodeId): ManagerResults[Graph] = for {
    node <- getNode(g, id1)
    node2 <- getNode(g, id2)
    edge0 <- getConnectionType(node, node2)
    edge <- compareEdges(edge0, Disconnected)
  } yield g.setNode(node.setConnection(node2.id, Connected)).setNode(node2.setConnection(node.id, Connected))

  private def compareEdges(e1: Edge, e2: Edge): ManagerResults[Edge] =
    if (e1 == e2) Right(e1) else Left(MismatchedConnection(e1, e2))

  def getNode(g: Graph, id: NodeId): ManagerResults[Node] = {
    g.nodes.get(id).map(Right.apply).getOrElse(Left(MissingNode(id)))
  }

  private def getConnection(n: Node, id: NodeId): ManagerResults[Edge] = {
    n.connections.get(id).map(Right.apply).getOrElse(Left(MissingConnection(n.id, id)))
  }

  private def getConnectionType(node1: Node, node2: Node): ManagerResults[Edge] =
    for {
      edge1 <- getConnection(node1, node2.id)
      edge2 <- getConnection(node2, node1.id)
      edge <- compareEdges(edge1, edge2)
    } yield edge

  def emptyNode(n: Node): ManagerResults[Unit] =
    if (n.status == NodeStatus.Empty) Right(()) else Left(NotEmptyNode(n.id))
}
