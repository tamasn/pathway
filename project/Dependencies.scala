import sbt._

object Dependencies {
  private val cats = "org.typelevel" %% "cats-core" % "1.0.0-MF"
  private val specs2 = "org.specs2" %% "specs2-core" % "3.9.5"

  val pathwayDeps = Seq(cats, specs2 % "test")
}

object ScalacOptions {
  private val specs2 = Seq("-Yrangepos")

  val pathwayScopts = specs2
}
