addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.0")

addSbtPlugin("org.wartremover" % "sbt-wartremover" % "2.2.0")
