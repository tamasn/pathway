import Dependencies._
import ScalacOptions._

import com.typesafe.sbt.SbtScalariform.ScalariformKeys
import scalariform.formatter.preferences._

val formatting =
  ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference(AlignSingleLineCaseStatements, true)
    .setPreference(DoubleIndentConstructorArguments, true)
    .setPreference(DanglingCloseParenthesis, Preserve)

lazy val pathway = (project in file("."))
	.settings(
		name := "pathway",
		version := "0.1",
		scalaVersion := "2.12.3",
		libraryDependencies ++= pathwayDeps,
		scalacOptions ++= pathwayScopts,
		wartremoverErrors ++= Warts.allBut(Wart.Product, Wart.Serializable, Wart.Equals, Wart.Nothing, Wart.Any, Wart.Recursion),
		Seq(formatting))
